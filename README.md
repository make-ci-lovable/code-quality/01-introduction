# code-quality-experiments ...

> - https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool
```json
[
  {
    "description": "'unused' is assigned a value but never used.",
    "fingerprint": "7815696ecbf1c96e6894b779456d330e",
    "location": {
      "path": "lib/index.js",
      "lines": {
        "begin": 42
      }
    }
  }
]
```
this is an update